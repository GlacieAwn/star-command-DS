# Star Command DS
Star Command DS is a port of a turret-style arcade shooter that I wrote originally for modern PCs with lua and love2D. 
the concept is pretty simple. move left and right, and shoot comets. you may notice that this looks a little different than
the PC port, and that will be resolved after I'm done developing this port, as I will rewrite the PC port in C with SDL2. 

# Dependencies
- blocksDS
- libNDS
- GNU Make

# Build Instructions
once all the dependencies are installed, you can build the DS rom by typing `make` in your terminal on linux, and mingw on windows.
the resulting DS rom can be played on emulator, and on real hardware using an R4 flash card.

# Special Thanks
this wouldn't be possible without the amazing gbadev community who helped me with any questions I had about the DS. 
