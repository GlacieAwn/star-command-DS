#include "include/comet.h"
#include "include/utils.h" // for randRange
#include "include/player.h"
#include "comet.h" // comet graphic

void initComet(struct Comet *this){
	// store off-screen until ready(also known as $FF)
	this->w = 16;
	this->h = 16;
	this->x = randRange(0, 256 - this->w);
	this->y = 0 - this->h;
	this->speed = 40.0f;
	this->speedMod = 0.005f;
	this->index = 1;
	this->priority = 0;
	this->delay = 0;
	this->state = COMET_INACTIVE;

	this->hitbox = (rect){this->x, this->y, this->w, this->h};

	this->tileBuff =  oamAllocateGfx(&oamMain, SpriteSize_16x16, SpriteColorFormat_256Color);
	dmaCopy(cometTiles, this->tileBuff, cometTilesLen);

}

void updateComet(struct Comet *this, struct Player *player, float dt){
	switch(this->state){
		case COMET_INACTIVE:
			// store comet off-screen when inactive
			this-> y = 0 - this->h;
			this->x = randRange(0, 256 - this->w);

			this->state = COMET_ACTIVE;
			break;
			
		case COMET_ACTIVE:
			this->y += this->speed * dt;

			this->speed += this->speedMod;

			// cap speed 
			if(this->speed >= 80){
				this->speed = 80;				
			}

			// check for collision with the bottom screen. for now, comet will just reset back to the top, however later
			// this will be used in conjunction with checking the player lives variable to trigger a game over if 
			// player lives is 0.
			if(this->y >= 192){
				resetComet(this);
				player->state = PLAYER_DEAD;
			}			
	}	
			// update the hitbox
			this->hitbox = (rect){this->x, this->y, this->w, this->h};
}

void resetComet(struct Comet *this){
	this->y = -16;
	this->x = randRange(0, 256 - this->w);
	this->state = COMET_INACTIVE;	
}
void drawComet(struct Comet *this){
		
	oamSet(&oamMain, this->index, this->x, this->y, this->priority, 0, SpriteSize_16x16, SpriteColorFormat_256Color, this->tileBuff, -1, false, false, false, false, false);
	oamSetXY(&oamMain, 1, this->x, this->y);
}


