#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// custom types
typedef struct {
	int x, y;
	int w, h;
} rect;

typedef struct {
	int x, y;
} point;

// function templates for helpful engine functions
int randRange(int l, int h);
bool checkCollisionRect(rect a, rect b);
