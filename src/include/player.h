#pragma once

#include <nds.h>
#include <nds/arm9/math.h>
#include <maxmod9.h>

#include "comet.h"
#include "utils.h"

// player states
enum PLAYER_STATE {
	PLAYER_IDLE,
	PLAYER_MOVING,
	PLAYER_DEAD
};
struct Player {
	float x, y;
	int w, h;
	float speed;
	float vel;
	float accel;
	int index;
	int priority;
	int lives;
	u16* tileBuff;
	rect hitbox;

	enum PLAYER_STATE state;
		
};

void initPlayer(struct Player *this);
void updatePlayer(struct Player *this, struct Comet *comet, int btn, int btn_press, float dt);
void drawPlayer(struct Player *this);

