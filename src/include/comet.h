#pragma once

#include <nds.h>
#include <stdbool.h>

#include "player.h"
#include "utils.h"

enum COMET_STATE{
	COMET_INACTIVE,
	COMET_ACTIVE	
};
struct Comet {
	float x, y;
	int w, h;
	float speed;
	float speedMod;
	int index;
	int priority;
	int delay;
	
	bool visible;
	u16* tileBuff;
	rect hitbox;

	enum COMET_STATE state;
};

void initComet(struct Comet *this);
void updateComet(struct Comet *this, struct Player *player, float dt);
void resetComet(struct Comet *this);
void drawComet(struct Comet *this);
