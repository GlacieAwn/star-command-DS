#pragma once

#include <stdio.h>
#include <time.h>
#include <nds.h>
#include <nds/arm9/math.h>
#include <maxmod9.h>

enum GAME_STATE {
	SPLASH,
	TITLE,
	GAMEPLAY,
	GAMEOVER	
};
struct Game {
	int score;
	int state;
};
void Start();
void Update();
void Draw();
