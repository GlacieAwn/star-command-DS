#pragma once

#include <nds.h>
#include <maxmod9.h>
#include <stdbool.h>

// include the necessary structs to pass into arguments
#include "player.h"
#include "comet.h"
#include "utils.h"
#include "game.h"

enum BULLET_STATE {
	BULLET_INACTIVE,
	BULLET_ACTIVE	
};

struct Bullet {
	int x, y;
	int w, h;
	int index;
	int priority;
	u16* tileBuff;
	rect hitbox;
	enum BULLET_STATE state;
};

void initBullet(struct Bullet *this);
void updateBullet(struct Bullet *this, struct Player *player, struct Comet *comet, struct Game *game, int btn);
bool checkCollision(struct Bullet *this, struct Comet *comet);
void drawBullet(struct Bullet *this);
void resetBullet(struct Bullet *this);
