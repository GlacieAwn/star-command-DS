#include <nds.h>
#include <maxmod9.h>
#include "game.h"

enum GAME_STATE {
	SPLASH,
	TITLE,
	GAMEPLAY,
	GAME_OVER	
};

void Splash();
void Title();
void Gameplay();
void GameOver();
