#include "include/game.h"
#include "include/player.h"
#include "include/comet.h"
#include "include/bullet.h"

#include "soundbank.h"
#include "soundbank_bin.h"
#include "bg.h" // background bitmap

struct Player player;
struct Comet comet;
struct Bullet bullet;
struct Game game;

void Start(){
	consoleDemoInit();

	mmInitDefaultMem((mm_addr)soundbank_bin);

	// set default video mode and vram bank A
	videoSetMode(MODE_5_2D);
	vramSetBankA(VRAM_A_MAIN_SPRITE && VRAM_A_MAIN_BG);
		
	oamInit(&oamMain, SpriteMapping_1D_32, false);
	int bg = bgInit(3, BgType_Bmp8, BgSize_B8_256x256, 0, 0);

	dmaCopy(bgBitmap, bgGetGfxPtr(bg), bgBitmapLen);
	dmaCopy(bgPal, BG_PALETTE, bgPalLen);
	// init objects
	initPlayer(&player);
	initComet(&comet);
	initBullet(&bullet);

	// generate a new random seed at the beginning of the game(move to title screen later)
	srand(time(NULL));
}

void Update(){
	swiWaitForVBlank();

	// fixed delta time to muptiply game logic by
	float dt = 1.0f / 60.0f;
	floattof32(dt);
	// clear console
	printf("\x1b[2J");
	printf("SCORE: %i\n\nLIVES: %i\n\n\n\n", game.score, player.lives);
	printf("CONTROLS \n \n");
	printf("Left Dpad: move left\n");
	printf("Right Dpad: move right\n");
	printf("A: Shoot\n");
	printf("Start: return to loader\n\n");
	
	// printf("DEBUG INFO \n\n");
	// printf("Delta Time: %f\n", dt);
	// printf("Player State %i\n", player.state);
	// printf("Player X: %f\n", player.x);
	// printf("Player Lives: %i\n", player.lives);
	// printf("Bullet State: %i\n", bullet.state);
	// printf("Comet X: %f\n", comet.x);
	// printf("Comet Y: %f\n", comet.y);
	// printf("Comet Delay: %i\n", comet.delay);
	// printf("Comet Speed: %f\n", comet.speed);
	// printf("Comet State: %i\n", comet.state);
	// 
	// printf("Bullet Hitbox: %i, %i, %i, %i, \n", bullet.hitbox);
	// printf("Comet Hitbox: %i, %i, %i, %i \n", comet.hitbox);
	scanKeys();
	int btn_held    = keysHeld();
	int btn_pressed = keysDown();

	if(btn_pressed == KEY_START){
		exit(0);
	}
	
	updatePlayer(&player, &comet, btn_held, btn_pressed, dt);
	updateComet(&comet, &player, dt);
	updateBullet(&bullet, &player, &comet, &game, btn_pressed);	
	// update OAM after all objects have been updated
	oamUpdate(&oamMain);
}

void Draw(){
	drawPlayer(&player);
	drawComet(&comet);
	drawBullet(&bullet);
}
