#include "include/player.h"
#include "include/comet.h"
#include "ship.h"

void initPlayer(struct Player *this){
	// set x and y to the middle of the screen for now for debug purposes, as well as set up width and height
	this->w = 16;
	this->h = 8;
	this->x = 256 / 2 - this->h;
	this->y = 192 - this->h - 8;
	this->vel = 0;
	this->accel = 10;
	this->speed = 1.5;
	this->index = 0; // sprite index
	this->priority = 0; // priority (in front of background)
	this->lives = 3;
	this->state = PLAYER_IDLE;

	// allocate space for tiles and copy them to vram
	this->tileBuff = oamAllocateGfx(&oamMain, SpriteSize_16x8, SpriteColorFormat_256Color);
	dmaCopy(shipTiles, this->tileBuff, shipTilesLen);

	// copy palette to VRAM
	dmaCopy(shipPal, SPRITE_PALETTE, shipPalLen);

	floattof32(this->x);
	floattof32(this->y);
	floattof32(this->speed);	

}

void updatePlayer(struct Player *this, struct Comet *comet, int btn, int btn_press, float dt){
	switch(this->state){
		case PLAYER_IDLE:
			if(btn & KEY_LEFT || btn & KEY_RIGHT){
				this->state = PLAYER_MOVING;
			}
			else{
				this->state = PLAYER_IDLE;
			}
			break;
			
		case PLAYER_MOVING:
			// change velocity and acceleration on dpad left/right
			if(btn & KEY_LEFT){
				this->vel -= this->accel * dt;
			}
			else if(btn & KEY_RIGHT){
				this->vel += this->accel * dt;
			}

			// velocity cap
			else if(this->vel > 0 ){
				this->vel -= this->accel * dt;
			    if(this->vel < 0){
			        this->vel = 0;
			    }
			}
		    else if(this->vel < 0){
		        this->vel += this->accel * dt;
		        if(this->vel > 0){
		            this->vel = 0;
		        }
		    }

		    // speed cap
		    if(this->vel > this->speed){
		    	this->vel = this->speed;
		    }
		    else if(this->vel < -this->speed){
		    	this->vel = -this->speed;
		    }

		    // add velocity to the x after it gets calculated
		    this->x += this->vel;
			
			// manual screen wrapping. the DS does have sprite wrapping, however it's not like the NES where it wraps to the beginning once you go past 0xFF(I.E, when it resets to 1). the DS has a 256x192 display, however there's an extra 256px off-screen. therefore, accurate screen wrapping has to be manually calculated by checking the left and right side of the screen offset by the ship's width on either side.
			
			if(this->x >= 256){
				this->x = 0 - this->w;
			} else if(this->x <= 0 - this->w){
				this->x = 256;
			}

			// if velocity is 0, change game state back to idle(aka, if the dpad buttons are released)
			if(this->vel == 0){
				this->state = PLAYER_IDLE;
			}

			break;
						
		case PLAYER_DEAD:
			if(this->lives <= 1){
				this->x = 0xff;
				this->y = 0xff;
				comet->y = 0xff;
				comet->x = 0xff;
				comet->speed = 0;
			}
			else {
				this->lives--;
				this->state = PLAYER_MOVING;
			}
		}
	
}

void drawPlayer(struct Player *this){

	oamSet(&oamMain, 0, 0xff, 0xff, this->priority, this->index, SpriteSize_16x8, SpriteColorFormat_256Color, this->tileBuff, -1, false, false, false, false, false);

	oamSetXY(&oamMain, 0, this->x, this->y);
}
