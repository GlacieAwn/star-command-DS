#include "include/utils.h"


int randRange(int l, int u){
	return (rand() % (u - l + 1)) + l;
}


bool checkCollisionRect(rect a, rect b){

	return a.x + a.w >= b.x && a.x <= b.x + b.w && a.y + a.h >= b.y && a.y <= b.y + b.h; 
}
