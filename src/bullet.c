#include "include/bullet.h"
#include "include/utils.h"
#include "include/comet.h" // for destroyComet();
#include "include/player.h" // to acces player state
#include "include/game.h"
#include "bullet.h" // bullet graphics

void initBullet(struct Bullet *this){
	this->w = 4;
	this->h = 4;
	// store offscreen($FF)
	this->x = 0xff;
	this->y = 0xff;
	this->index = 2;
	this->priority = 0;
	this->state = BULLET_INACTIVE;
	this->hitbox = (rect){this->x, this->y, this->w, this->h};

	this->tileBuff = oamAllocateGfx(&oamMain, SpriteSize_8x8, SpriteColorFormat_256Color);
	dmaCopy(bulletTiles, this->tileBuff, bulletTilesLen);
}

void updateBullet(struct Bullet *this, struct Player *player, struct Comet *comet, struct Game *game, int btn){
	switch(this->state){
		case BULLET_INACTIVE:
		
			if(btn & KEY_A){
				this->y = player->y;
				this->x = player->x;
				this->state = BULLET_ACTIVE;
			}
			break;
		case BULLET_ACTIVE:
			this->y -= 2;

			if(this->y <= 0){
				resetBullet(this);
				this->state = BULLET_INACTIVE;
			}

			// check collision
			if(checkCollisionRect(this->hitbox, comet->hitbox) && comet->state == COMET_ACTIVE){
				resetComet(comet);
				resetBullet(this);
				game->score += 100;
			}
	}

		this->hitbox = (rect){this->x, this->y, this->w, this->h};
}


void resetBullet(struct Bullet *this){
	this->x = 255;
	this->y = 255;
	this->state = BULLET_INACTIVE;
}
void drawBullet(struct Bullet *this){
	oamSet(&oamMain, this->index, this->x, this->y, this->priority, 0, SpriteSize_8x8, SpriteColorFormat_256Color, this->tileBuff, -1, false, false, false, false, false);
	oamSetXY(&oamMain, this->index, this->x, this->y);
}
